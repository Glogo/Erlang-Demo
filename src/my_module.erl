%
%	@author Glogo4j
%


%% This is a simple Erlang module.

%% module defination, '-' must be first.

%% the name of a module must be same as file's.


-module(my_module).		

%% export the function you have defined in a list as a parameter.

-export([pie/0, print/1, either_or_both/2, area/1, area2/1]).


pie() ->
	3.14.

print(Term) ->
	io:format("The value of term is : ~w. ~n", [Term]).	


%% I don't know which style below is proper.

% either_or_both(true, _) ->
% 	true;
% 	either_or_both(_, true) ->
% 		true;
% 		either_or_both(false, false) ->
% 			false.


%% function clause and Guard
either_or_both(true, A) when is_boolean(A) ->		% is_boolean() is a built-in function, BIF
	true;
either_or_both(B, true) when is_boolean(B) ->	% such as is_atom(), is_integer(),designed for basic type
	true;
either_or_both(false, false) ->
	false.

%% function clause
area({circle, Radius}) ->	
	Radius * Radius * math:pi();
area({square, Side}) ->
	Side * Side;
area({rectangle, Height, Width}) ->
	Height * Width.

%% case clause
area2(Shape) ->
	case Shape of
		{circle, Radius} ->
			Radius * Radius * math:pi();
		{square, Side} ->
			Side * Side;
		{rectangle, Height, Width} ->
			Height * Width
	end.