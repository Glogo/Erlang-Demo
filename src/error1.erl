%
% Erlang并发编程中的错误处理
%

-module (error1).

-export ([on_exit/2]).

% Two processes, if one calls the BIF link(P) (ps:P is another process's Pid),both of them are linked,
% They began to monitor each other.
% If A die,system will send a exit signal to B. And vice versa.

on_exit(Pid, Fun) ->
	spawn(fun() -> 
			process_flag(trap_exit, true),	% 把创建的进程变成一个系统进程
			link(Pid),		% 把新建进程与Pid指示的进程相互链接,那么当Pid进程消亡时,就会发送消息,后续语句开始工作
			receive
				{'EXIT', Pid, Why} -> 
					Fun(Why)
			end
		end). 


% 如何构建一个容错系统 %

% 常规来说需要两台计算机,一台正常工作,一台监视并在故障发生的时候接管工作。
% 这也是Erlang的工作机制,一个进程正常工作,另一个进程监视其工作,并在发生错误时接管该工作。
% 在分布式Erlang中，具体的工作进程和监视进程可能分布在物理位置上不同的机器上，利用这种特性，我们可以开始设计容错系统