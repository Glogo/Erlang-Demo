% 一个定时器，在shell下传入指定时间与fun函数后会在规定时间内调用

% 其实利用 receive after end 

-module(stimer).

-export([start/2, cancel/1]).



start(Time, Fun) -> spawn(fun() -> timer(Time, Fun) end ).

timer(Time, Fun) ->
	receive
		cancel -> void
	after Time ->
		Fun()
	end.



cancel(Pid) -> Pid ! cancel.