%% 尾递归技术

%% Erlang中经常使用尾递归代替迭代,但不是说尾递归就比迭代有优势.

-module (recursion).

-export ([sum/1, do_sum/1]).

%% 非尾递归
sum(N) when N =/= 0 -> sum(N - 1) + N;
sum(0) -> 0.

%% 尾递归
do_sum(N) ->  do_sum(N, 0).

do_sum(0, Total) -> Total;
do_sum(N, Total) -> do_sum(N -1, Total + N).


%%就用这个程序计算100000000000,要算老半天才算出来.