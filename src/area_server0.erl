
% 创建进程，spawn(Fun)函数，对Fun求值，并返回Pid
% 然后就是接收消息，receive到end块，对邮箱中的消息进行模式匹配，然后 do_something.
% 这里其实是在循环监听。

-module(area_server0).

-export([loop/0]).

loop() ->
	receive
		{From, {rectangle, Width, Height} } ->
			io:format("Area of rectangle is ~p~n", [Width * Height]),
			From ! Width * Height,
			loop();
		{From, {circle, R} } ->
			io:format("Area of circle is ~p~n", [3.14159 * R * R]),
			From ! 3.14159 * R * R,
			loop();
		Other ->
			io:format("I don't know what the area of a ~p~n", [Other]),
			loop()
	end.
