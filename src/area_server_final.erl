% area_server的final版本

-module (area_server_final).

-export ([start/0, area/2]).



start() -> spawn(fun loop/0).

area(Pid, What) -> rpc(Pid, What).

rpc(Pid, Request) ->
	Pid ! {self(), Request},
	receive
		{Pid, Response} -> Response
	end.

loop() ->
	receive
		{From, {rectangle, Width, Height}} ->
			% io:format("Area of rectangle is ~p~n", [Width * Height]),
			From ! {self(), Width * Height},
			loop();
		{From, {circle, R}} ->
			% io:format("Area of circle is ~p~n", [3.14159 * R * R]),
			From ! {self(), 3.14159 * R * R},
			loop();
		{From, Other} ->
			% io:format("I don't know what the area of a ~p~n", [Other]),
			From ! {self(), {error, Other}},
			loop()
	end.

