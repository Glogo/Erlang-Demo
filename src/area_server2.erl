-module(area_server2).

-export([loop/0, rpc/2]).

%% server!!!
loop() ->
	receive
		{From, {rectangle, Width, Height}} ->
			% io:format("Area of rectangle is ~p~n", [Width * Height]),
			From ! {self(), Width * Height},
			loop();
		{From, {circle, R}} ->
			% io:format("Area of circle is ~p~n", [3.14159 * R * R]),
			From ! {self(), 3.14159 * R * R},
			loop();
		{From, Other} ->
			% io:format("I don't know what the area of a ~p~n", [Other]),
			From ! {self(), {error, Other}},
			loop()
	end.

%% local client!!!
rpc(Pid, Request) ->
	Pid ! {self(), Request},
	receive
		{Pid, Response} -> 
			Response
	end.