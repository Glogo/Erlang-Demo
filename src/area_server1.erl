% 客户机/服务器

-module(area_server1).

-export([loop/0, rpc/2]).

%% server
loop() ->
	receive
		% From是客户机的Pid,
		{From, {rectangle, Width, Height}} ->
			% io:format("Area of rectangle is ~p~n", [Width * Height]),
			From ! {self(), Width * Height},	% 向From发送消息
			loop();
		{From, {circle, R}} ->
			% io:format("Area of circle is ~p~n", [3.14159 * R * R]),
			From ! {self(), 3.14159 * R * R},	% 向From发送消息
			loop();
		{From, Other} ->
			% io:format("I don't know what the area of a ~p~n", [Other]),
			From ! {self(), error, Other},		% 向From发送消息
			loop()
	end.

% local client
% Pid 是服务器的Pid
% 在发从消息但时候一并将自己但Pid发送过去给服务器
rpc(Pid, Request) ->
	Pid ! {self(), Request},
	receive
		{Pid, Response} -> 
			Response
	end.