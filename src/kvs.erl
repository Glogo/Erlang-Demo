% 以下来自于《Erlang程序设计》
%
% 分布式编程：是指仅通过消息传递来完成相互协作的计算机集群所设计的程序
%
% 编写分布式应用的需求
% 1.效率：我们把程序分成多个部分，并行运行在不同的机器上，这样可以让程序跑的更快
% 2.可靠性：我们把一个系统分布运行到若干机器上，我们可以构建容错系统。如果某台计算机崩溃，那么这个系统会由其他计算机接替运行
% 3.可伸缩性：由于我们会不断的扩展应用程序，再强大的计算机也会面临资源枯竭，在这种情况下，我们必须能够通过增加机器来扩充容量，
% 		增加机器是一件相对容易的事情，而不需要伤筋动骨的去修改应用程序
% 4.天生需要分布式的应用程序：很多程序需要天生具有分布性，比如说网络游戏，或者聊天系统。如果在某个地区，用户群足够大，那么我
% 		们就会倾向于在靠近用户的地方配置更多的资源。
% 5.乐趣：xxxx


% 名字服务——我们向一个服务提交一个名字，服务器就向我们返回与这个名字相关的值，这种服务就叫名字服务。

-module(kvs).

-export([start/0, store/2, lookup/1]).


start() -> register(kvs, spawn(fun() -> loop() end) ).
	

store(Key, Value) -> rpc({store, Key, Value}).


lookup(Key) -> rpc({lookup, Key}).


rpc(Q) -> 
	kvs ! {self(), Q},
	receive
		{kvs, Reply} -> Reply
	end.


loop() ->
	receive
		{From, {store, Key, Value}} ->
			put(Key, {ok, Value}),
			From ! {kvs, true},
			loop();
		{From, {lookup, Key}} ->
			From ! {kvs, get(Key)},
			loop()
	end.

% ********** 以下结果为在终端运行的显示 **********
%  2> c(kvs).
% {ok,kvs}
% 3> kvs:start().
% true
% 4> kvs:store({location, joe}, "Stockholm").
% true
% 5> kvs:store(weather, raining).            
% true
% 6> kvs:lookup(weather).        
% {ok,raining}
% 7> kvs:lookup({location,joe}).
% {ok,"Stockholm"}
% 8> kvs:lookup({location,jane}).
% undefined


% 函数rpc:call(Node, Mod, Func, [Arg1, Arg2, Arg3...])用于在一个Node上执行远程调用。

% 节点——分布式Erlang的核心概念就是节点，一个节点就是一个自给自足的系统，它是一个包含了地址空间和独立进程集的完整虚拟机
% 访问单个节点和节点集都要收到cookie系统的保护，每个节点有一个cookie，而且必须保证所有要和这个节点通信的其他节点都有相同的cookie。
% 具有相同的cookie，并且彼此间链接起来的节点集称为Erlang集群。




