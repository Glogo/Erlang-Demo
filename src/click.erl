% 每隔指定时间便调用一次传入的fun函数，
% 通过向进程发送消息可以停止。

-module(click).

-export([start/2, stop/0]).

start(Time, Fun) ->	register(clock, spawn(fun() -> tick(Time, Fun) end)).
	
tick(Time, Fun) -> 
	receive
		stop -> void
	after Time ->
		Fun(),
		tick(Time, Fun)
	end.

stop() -> whereis(clock) ! stop.