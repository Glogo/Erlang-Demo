%
% Erlang并发程序的一般化模板
% 

-module(concurrency_demo).

-compile(export_all).

start() -> spawn(fun() -> loop([]) end).

rpc(Pid, Request) ->
	Pid ! {self(),Request},
	receive 
		{Pid, Response} ->
			Response
	end.


loop(X) ->
	receive 
		Any ->
			io:format("Received:~p~n",[Any]),
			loop(X)
	end.